#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

die()
{
    echo "$@" 1>&2
    exit 1
}

usage()
{
    local scriptname=$0
    cat <<EOF

Helper script for packaging Koha.

Usage:

  $scriptname [--basetgz-only]

Options:

  --basetgz-only       Only build the base.tgz file, useful for reusing
  --help,-h            Show this help.

EOF
}

# Prevents gethostbyname error
export WEBSERVER_IP='*'

git config --global --add safe.directory /koha

if [ ! -f /var/cache/pbuilder/base.tgz ]; then
	# Build base.tgz
	echo "BUILDING base.tgz FILE"
	apt-get update
	pbuilder create  --use-network yes --distribution buster --mirror http://debian.csail.mit.edu/debian/
	pbuilder execute --use-network yes --save-after-exec /pbuilder.sh --basetgz /var/cache/pbuilder/base.tgz
else
	echo "BUILDING base.tgz FILE => skipped, file exists"
fi

echo "RESULTING base.tgz"
ls -alh /var/cache/pbuilder/base.tgz

BASETGZ_ONLY="no"

while true ; do
    case "$1" in
        --basetgz-only)
            BASETGZ_ONLY="yes" ; shift ;;
        -h|--help)
            usage ; exit 0 ;;
        --)
            shift ; break ;;
        *)
            break;;
    esac
done

if [ "$BASETGZ_ONLY" != "yes" ]; then
	cd /koha

	if [ -z "${VERSION}" ]; then
		VERSION_OPTIONS="--autoversion"
	else
		VERSION_OPTIONS="-v ${VERSION} --noautoversion"
	fi

    echo "USENETWORK=yes" >> /etc/pbuilderrc

	echo "BUILDING DEBIAN PACKAGE"
	./debian/build-git-snapshot \
		--basetgz base \
		-r /debs \
		${VERSION_OPTIONS} -d
fi
