FROM debian:buster-slim

LABEL maintainer="tomascohen@theke.io"

# Valid: master, major.minor (19.11, etc)
ARG BRANCH

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin:/
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update \
    && apt-get -y --allow-unauthenticated install \
      bash-completion \
      build-essential \
      debian-archive-keyring \
      devscripts \
      dh-make \
      docbook-xsl-ns \
      fakeroot \
      git \
      gnupg \
      gnupg2 \
      libmodern-perl-perl \
      pbuilder \
      wget \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/api/lists/*

RUN mkdir -p /var/cache/pbuilder/
# COPY base.tgz /var/cache/pbuilder/base.tgz

VOLUME /debs
VOLUME /koha
VOLUME /output

ENV PERL5LIB=/koha

COPY pbuilder.sh /pbuilder.sh
ADD build.sh /build.sh

CMD /build.sh
