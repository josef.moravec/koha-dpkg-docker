#!/bin/bash

# RELEASE can be set to any of https://debian.koha-community.org/koha/dists/
if [[ -z "${RELEASE}" ]]; then
    echo "NO RELEASE ENVIRONMENT VARIABLE SET"
    echo "RELEASE can be set to any of https://debian.koha-community.org/koha/dists/"
    exit 1;
else
    echo "RELEASE: $RELEASE"
fi

apt update
apt install -y wget gnupg ca-certificates
wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -

if [ "$RELEASE" == "master" ]; then
    echo "deb http://debian.koha-community.org/koha-staging dev main" >> /etc/apt/sources.list.d/koha.list
else
    echo "deb http://debian.koha-community.org/koha $RELEASE main buster" > /etc/apt/sources.list.d/koha.list
fi

# Add Node.js
wget -O- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
wget -O- -q https://deb.nodesource.com/setup_14.x | bash -
# Pin Node.js v14
echo "Package: nodejs"   >> /etc/apt/preferences.d/nodejs
echo "Pin: version 14.*" >> /etc/apt/preferences.d/nodejs
echo "Pin-Priority: 999" >> /etc/apt/preferences.d/nodejs
# Add yarn repo
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
wget -O- -q https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null 
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list

apt update
apt install -y \
     koha-perldeps \
     bash-completion \
     docbook-xsl-ns \
     libtest-dbix-class-perl \
     libdata-uuid-perl \
     nodejs \
     yarn
apt clean

node --version

npm install -g gulp-cli
